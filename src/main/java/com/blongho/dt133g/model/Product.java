/**
 * ThesisData io.blongho.thesis.model Product.java
 * 
 * longb May 19, 2019
 * 
 */
package com.blongho.dt133g.model;

/**
 * The type Product<br>
 * A product has a name and a description
 *
 * @author Bernard Che Longho
 * @version 1.0
 * @since 2019-05-10
 */
public class Product {

	private String description;

	private String name;

	private long id;

	/**
	 * Instantiates a new Product.
	 */
	public Product() {
	}

	/**
	 * Instantiates a new Product.
	 *
	 * @param product_id  the product id
	 * @param name        the name
	 * @param description the description
	 */
	public Product(final long product_id, final String name, final String description) {
		this.id = product_id;
		this.name = name;
		this.description = description;
	}

	/**
	 * Gets description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets description.
	 *
	 * @param description the description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets name.
	 *
	 * @param name the name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets product id.
	 *
	 * @return the product id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets product id.
	 *
	 * @param id the product id
	 */
	public void setId(final long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Product{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}

}
