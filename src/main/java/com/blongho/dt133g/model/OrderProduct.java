/**
 * ThesisData io.blongho.thesis.model OrderProduct.java
 * 
 * longb May 19, 2019
 * 
 */
package com.blongho.dt133g.model;

/**
 * The type Order product.
 */
public class OrderProduct {

	private long product;

	private long id;

	private long order;

	/**
	 * Instantiates a new Order product.
	 */
	public OrderProduct() {
	}

	/**
	 * Instantiates a new Order product.
	 *
	 * @param orderProduct_id the order product id
	 * @param product_id      the customer id
	 * @param order_id        the order id
	 */
	public OrderProduct(final long orderProduct_id, final long order_id, final long product_id) {
		this.id = orderProduct_id;
		this.order = order_id;
		this.product = product_id;
	}

	/**
	 * @return the product
	 */
	public long getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(long product) {
		this.product = product;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the order
	 */
	public long getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(long order) {
		this.order = order;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("OrderProduct{");
		sb.append("orderProduct_id=").append(id);
		sb.append(", product_id=").append(product);
		sb.append(", order_id=").append(order);
		sb.append('}');
		return sb.toString();
	}

}
