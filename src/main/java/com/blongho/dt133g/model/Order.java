/**
 * ThesisData io.blongho.thesis.model Order.java
 * 
 * longb May 19, 2019
 * 
 */
package com.blongho.dt133g.model;

import java.util.Date;
import java.util.List;

import org.eclipse.jdt.annotation.Nullable;

/**
 * The type Order<br>
 * An order holds an identifier to the customer and the list of products
 * ordered.
 *
 * @author Bernard Che Longho
 * @version 1.0
 * @since 2019-05-10
 */
public class Order {

	private long customer;

	private Date date;

	private long id;

	private List<Product> products;

	/**
	 * Instantiates a new Order.
	 */
	public Order() {
	}

	/**
	 * Instantiates a new Order.
	 *
	 * @param order_id    the order id
	 * @param customer_id the customer id
	 */
	public Order(final long order_id, final long customer_id, @Nullable
	final Date date) {
		this.id = order_id;
		this.customer = customer_id;
		if (date == null) {
			this.date = new Date();
		}
		this.date = date;
	}

	/**
	 * Gets customer id.
	 *
	 * @return the customer id
	 */
	public long getCustomerd() {
		return customer;
	}

	/**
	 * Sets customer id.
	 *
	 * @param customer_id the customer id
	 */
	public void setCustomer(final long customer_id) {
		this.customer = customer_id;
	}

	/**
	 * Gets date.
	 *
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Sets date.
	 *
	 * @param date the date
	 */
	public void setDate(final Date date) {
		this.date = date;
	}

	/**
	 * Gets order id.
	 *
	 * @return the order id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets order id.
	 *
	 * @param id the order id
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * Gets products.
	 *
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * Sets products.
	 *
	 * @param products the products
	 */
	public void setProducts(final List<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Order{");
		sb.append("id=").append(id);
		sb.append(", customer=").append(customer);
		sb.append(", products=").append(products);
		sb.append('}');
		return sb.toString();
	}
}
