/**
 * ThesisData io.blongho.thesis Main.java
 * 
 * longb May 19, 2019
 * 
 */
package com.blongho.dt133g;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;
import java.util.concurrent.Executors;

import com.blongho.dt133g.extraction.DataSetGenerator;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-22
 * @brief Main class that runs the different data generators
 */
public class Main {
private final static String SAMPLES = "samples";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Call generateData(min, max, intervals);
		Executors.newCachedThreadPool().execute(() -> generateData(1000, 12000, 1000));

	}

	/**
	 * Generates data for Customer, Product, Order and OrderProduct
	 * <p>
	 * Generates json data sets like product# where # is number of entries
	 * in the file
	 * 
	 * @param min       the minimum number for the data
	 * @param max       the maximum number of entries
	 * @param intervals the interval
	 */
	static void generateData(final long min, final long max, final long intervals) {
		System.out.println("START: " + Calendar.getInstance().getTime());

		for (long number = min; number <= max; number += intervals) {
			DataSetGenerator.generateProducts(number);
			DataSetGenerator.generateCustomers(number);
			DataSetGenerator.GenerateOrders(number);
			DataSetGenerator.generateOrderProducts(number);
		}
		System.out.println("START: " + Calendar.getInstance().getTime());

	}

}
