/**
 * java-project com.blongho.thesis.thesis.extraction DataGenerator.java
 * 
 * longb May 22, 2019
 * 
 */
package com.blongho.dt133g.extraction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.blongho.dt133g.model.Customer;
import com.github.javafaker.Faker;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-21
 * @brief Generates {@linkCustomer} and returns a List<Customer>
 */
class GenerateCustomers extends Generator implements Callable<List<Customer>> {

	/**
	 * @param number
	 */
	public GenerateCustomers(long number) {
		super(number);
	}

	public GenerateCustomers() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public List<Customer> call() throws Exception {
		final List<Customer> customers = new ArrayList<>();
		final Faker faker = Faker.instance();
		for (long i = 1; i < number; i++) {
			customers.add(new Customer(i, faker.name().name(), faker.address().city()));
		}

		return customers;
	}

}
