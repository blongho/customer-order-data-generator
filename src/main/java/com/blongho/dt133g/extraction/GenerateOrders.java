/**
 * java-project com.blongho.thesis.thesis.extraction GenerateOrders.java
 * 
 * longb May 22, 2019
 * 
 */
package com.blongho.dt133g.extraction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.RandomUtils;

import com.blongho.dt133g.model.Order;
import com.github.javafaker.Faker;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-21
 * @brief Generates {@link Order} and returns a List<Order>
 */
class GenerateOrders extends Generator implements Callable<List<Order>> {

	private final long customers;

	/**
	 * 
	 */
	public GenerateOrders(final long customers) {
		super();
		this.customers = customers;
	}

	public GenerateOrders(final long numberOfOrders, final long customers) {
		super(numberOfOrders);
		this.customers = customers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public List<Order> call() throws Exception {
		final List<Order> orders = new ArrayList<>();
		final Faker faker = Faker.instance();
		for (long i = 1; i < number; i++) {
			orders.add(new Order(i, RandomUtils.nextLong(1L, customers), faker.date().birthday()));
		}
		return orders;
	}

}
