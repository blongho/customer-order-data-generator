/**
 * java-project com.blongho.thesis.thesis.extraction
 * GenerateProducts.java
 * 
 * longb May 22, 2019
 * 
 */
package com.blongho.dt133g.extraction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.blongho.dt133g.model.Product;
import com.github.javafaker.Book;
import com.github.javafaker.Faker;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-21
 * @brief Generates {@link Product} and returns a List<Product>
 */
class GenerateProducts extends Generator implements Callable<List<Product>> {
	/**
	 * 
	 */
	public GenerateProducts() {
		super();
	}

	public GenerateProducts(final long number) {
		super(number);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public List<Product> call() throws Exception {
		final List<Product> products = new ArrayList<>();
		final Faker faker = Faker.instance();
		Book book;
		for (long i = 1; i < number; i++) {
			book = faker.book();
			products.add(new Product(i, book.title(), book.author() + ", " + book.publisher()));
		}
		return products;
	}

}
