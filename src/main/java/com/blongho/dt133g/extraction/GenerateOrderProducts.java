/**
 * java-project com.blongho.thesis.thesis.extraction
 * GenerateOrderProducts.java
 * 
 * longb May 22, 2019
 * 
 */
package com.blongho.dt133g.extraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import com.blongho.dt133g.model.Order;
import com.blongho.dt133g.model.OrderProduct;
import com.blongho.dt133g.model.Product;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-21
 * @brief Generates {@link OrderProduct} and returns a
 *        List<OrderProduct>
 */
public class GenerateOrderProducts extends Generator implements Callable<List<OrderProduct>> {

	private final List<Order> orders;
	private final List<Product> products;

	/**
	 * @param number
	 * @param orders
	 * @param products
	 */
	public GenerateOrderProducts(long number, List<Order> orders, List<Product> products) {
		super(number);
		this.orders = orders;
		this.products = products;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public List<OrderProduct> call() throws Exception {
		final List<OrderProduct> orderProducts = new ArrayList<>();
		long order_id;
		long product_id;
		Collections.shuffle(orders);
		Collections.shuffle(products);
		final long ords = orders.size();

		for (int i = 1; i < number; i++) {
			if (i <= ords) {
				order_id = orders.get(i - 1).getId();
				product_id = products.get(i - 1).getId();
				orderProducts.add(new OrderProduct(i, order_id, product_id));
			}
		}
		return orderProducts;
	}

}
