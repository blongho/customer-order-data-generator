/**
 * java-project com.blongho.thesis.thesis.extraction GenerateData.java
 * 
 * longb May 22, 2019
 * 
 */
package com.blongho.dt133g.extraction;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

import org.eclipse.jdt.annotation.Nullable;

import com.blongho.dt133g.model.Customer;
import com.blongho.dt133g.model.Order;
import com.blongho.dt133g.model.OrderProduct;
import com.blongho.dt133g.model.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-21
 * @brief Utility class that encapsulates the generation of the
 *        different objects in the system
 */
public class DataSetGenerator {

	private static Executor executor = Executors.newCachedThreadPool();
	private static List<Customer> SYSTEM_CUSTOMERS = new ArrayList<>();
	private static List<Product> SYSTEM_PRODUCTS = new ArrayList<>();
	private static List<Order> SYSTEM_ORDERS = new ArrayList<>();
	private static List<OrderProduct> SYSTEM_ORDER_PRODUCTS = new ArrayList<>();

	/**
	 * 
	 */
	private DataSetGenerator() {

	}

	/**
	 * Generates {@customers} Customer objects
	 * 
	 * @param customers the number of Customers to generate
	 */
	public static void generateCustomers(long customers) {
		final CompletionService<List<Customer>> customerService = new ExecutorCompletionService<>(executor);
		customerService.submit(new GenerateCustomers(customers));
		try {
			SYSTEM_CUSTOMERS = customerService.take().get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		// customerList.forEach(System.out::println);
		toJson("customers" + customers, SYSTEM_CUSTOMERS);

	}

	/**
	 * Generates {@products} Products
	 * 
	 * @param products the number of Product to generate
	 */
	public static void generateProducts(long products) {
		final CompletionService<List<Product>> productService = new ExecutorCompletionService<>(executor);
		productService.submit(new GenerateProducts(products));
		try {
			SYSTEM_PRODUCTS = productService.take().get();

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		// See products
		// productList.forEach(System.out::println);
		toJson("products" + products, SYSTEM_PRODUCTS);
	}

	/**
	 * Generates {@number} of OrderProducts
	 * 
	 * @param number the number of OrderProducts to generate
	 */
	public static void generateOrderProducts(Long number) {
		final CompletionService<List<OrderProduct>> orderProductService = new ExecutorCompletionService<>(executor);
		orderProductService.submit(new GenerateOrderProducts(number, SYSTEM_ORDERS, SYSTEM_PRODUCTS));
		try {
			SYSTEM_ORDER_PRODUCTS = orderProductService.take().get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		toJson("order_products" + number, SYSTEM_ORDER_PRODUCTS);

	}

	/**
	 * Generates Orders specified by {@orders}
	 * 
	 * @param orders the number of orders to generate
	 */
	public static void GenerateOrders(long orders) {
		final CompletionService<List<Order>> orderService = new ExecutorCompletionService<>(executor);
		final long numberOfCustomers = SYSTEM_CUSTOMERS.size();
		orderService.submit(new GenerateOrders(orders, numberOfCustomers));

		try {
			SYSTEM_ORDERS = orderService.take().get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		toJson("orders" + orders, SYSTEM_ORDERS);
	}

	/**
	 * Saves items to a json file
	 * 
	 * @param fileName the file name
	 * @param items    the items to save to file
	 */
	private static <E> void toJson(@Nullable
	final String fileName, List<E> items) {
		if (items.isEmpty() || items == null) {
			System.err.println("Nothing to save to file");
			return;
		}
		final StringBuffer buffer = new StringBuffer();
		String empty;

		if (fileName == null || fileName.isEmpty()) {
			empty = items.get(0).getClass().getSimpleName().toLowerCase() + items.size();
			buffer.append("samples/").append(empty).append(".json");
		} else {
			buffer.append("samples/").append(fileName).append(".json");
		}
		final String path = buffer.toString();

		final File file = new File(path);
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
		}

		try (Writer writer = new FileWriter(file)) {
			final Gson gson = new GsonBuilder().create();
			gson.toJson(items, writer);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
}
