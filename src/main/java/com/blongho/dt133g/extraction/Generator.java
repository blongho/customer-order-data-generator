/**
 * java-project com.blongho.thesis.thesis.extraction Generator.java
 * 
 * longb May 22, 2019
 * 
 */
package com.blongho.dt133g.extraction;

/**
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @since 2019-05-21
 * @brief abstract base class for the different concrete generators
 */
abstract class Generator {
	protected final long number; // the number of products to be generated

	/**
	 * 
	 * @param number
	 */
	Generator(long number) {
		this.number = number + 1L;
	}

	Generator() {
		number = 1_000_1;
	}

}
