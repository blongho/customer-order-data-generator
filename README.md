# Customer order system data generator

This repository is a maven-based java project that generates and saves 
fictitious data that mimics a customer-order system. The data is generated
and saved in json files. 

This was developed as part of my Bachelor thesis in Software Engineering were 
i compared performance of Room and greenDAO as ORM frameworks for data 
persistence in Android. 

---

## System requirements
- [Eclipse IDE for Enterprise Java Developers](https://www.eclipse.org/downloads/packages/release/2019-03/r/eclipse-ide-enterprise-java-developers)
 or any IDE of your choice that supports Maven 3.5.4 and above (this is what i used for this project)
- [Java SDK 8 or greater](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

---
## Set up
### From IDE
1. Download or clone the repository
2. Import the project as a maven project `File>Import>{Maven}>Existing Maven Projects>Next>Browse/to/project/directory/>Next>Finish`
3. `Right click the project root > Maven > Update Project > Select project (if not selected) > Ok `
4. Go to `com/blongho/dt133g/Main`
5. Call `generateData(min, max, intervals)`;
6. Your files should be in the project root `samples/xxx###.json` 
 e.g. `samples/orders1000.json` which means 1000 orders in a json file 
 
### From terminal (As-IS)
 
 1. Download or clone this repository
 2. At the root of the project directory, run
 
 ```shell
 mvn install

 mvn exec:java

 # If everything works well, output

 START: Tue Oct 13 06:24:01 CEST 2020
START: Tue Oct 13 06:24:13 CEST 2020
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:21 min
[INFO] Finished at: 2020-10-13T06:25:13+02:00
[INFO] ------------------------------------------------------------------------

cd samples # generated data directory
ls samples # listing of the generated file

 ```


---

## References
- [java-faker](https://github.com/DiUS/java-faker)  that generates fake data. 
- [Gson](https://github.com/google/gson), Java serialization library to convert POJO to Json and back.

---
## Contribution guidelines
Feel free to fork the repository and or send any pull requests that you deem necessary

--- 

## Licence
MIT LicenSe. See [License](LICENSE)

---
## Contact
[Bernard Che Longho](mailto:lobe1602@student.miun.se) 2019


